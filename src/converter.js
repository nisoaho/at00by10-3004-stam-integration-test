const pad = (hex) => {
    return (hex.length === 1 ? '0' + hex : hex);
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redhex = red.toString(16) // kääntää arvon 16 bittiseksi
        const greenhex = green.toString(16)
        const bluehex = blue.toString(16)

        return pad(redhex) + pad(greenhex) + pad(bluehex)
    },

    hexToRgb: (hex) => {
        const h = "0123456789ABCDEF";
        var n = 0; // this is to handle the cases where there is # used with the hex
        
        if (hex.length === 7) {
            n = 1
        }

        // Make sure that even though hex is insterted in lower case, this will work.
        hex = hex.toUpperCase();
       
        // Note, will only work if the hex is given with 6 or 7 characters
        const red = h.indexOf(hex[0+n])*16+h.indexOf(hex[1+n]);
        const green= h.indexOf(hex[2+n])*16+h.indexOf(hex[3+n]);
        const blue = h.indexOf(hex[4+n])*16+h.indexOf(hex[5+n]);       
        return '('+ red + ', ' + green + ', ' + blue + ')'
    }
}