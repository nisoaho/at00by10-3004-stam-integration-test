const express = require('express');
const res = require('express/lib/response');
const { redirect } = require('express/lib/response');
const app = express();
const converter = require('./converter');
const port = 3000;

// endpoint localhost:3000, original version
app.get('/', (req, res) => res.send('Hello!')); 

// endpoint localhost:3000/rgb-to-hex?red=255&green=0&blue=0
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.red)
    const green = parseInt(req.query.green)
    const blue = parseInt(req.query.blue)

    const hex = converter.rgbToHex(red, green, blue);

    // Sending the converted value to the server
    res.send(hex);
})

// endpoint localhost:3000/hex-to-rgb?hex=ff0000
app.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex;
    const rgb = converter.hexToRgb(hex);
    // Sending the converted value to the server
    res.send(rgb);
})

if(process.env.NODE_ENV === 'test') {
    // Takes the module to mocha
    module.exports = app;
} else {
    app.listen(port, () => console.log(`Server listening on localhost:${port}`))
}

