// TDD - Teste Driven Development - Unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');

describe('Color code converter', () => {
    describe('RGB to Hex conversion', () => {
        it('converts the basic colors', () => {
            const redhex = converter.rgbToHex(255, 0, 0); // All red, in Hex ff0000
            const greenhex = converter.rgbToHex(0, 255, 0); // All red, in Hex 00ff00
            const bluehex = converter.rgbToHex(0, 0, 255); // All red, in Hex 0000ff

            expect(redhex).to.equal('ff0000')
            expect(greenhex).to.equal('00ff00')
            expect(bluehex).to.equal('0000ff')

        })
    })
    describe('Hex to RGB conversion', () => {
        it('converts the basic colors', () => {

            // Uppercase with #
            const redrgb = converter.hexToRgb('#FF0000'); // All red, in RGB (255, 0, 0)
            const greenrgb = converter.hexToRgb('#00FF00'); // All green, in RGB (0, 255, 0)
            const bluergb = converter.hexToRgb('#0000FF'); // All blue, in RGB (0, 0, 255)

            expect(redrgb).to.equal('(255, 0, 0)')
            expect(greenrgb).to.equal('(0, 255, 0)')
            expect(bluergb).to.equal('(0, 0, 255)')

            // Lower case without #
            const redrgbLow = converter.hexToRgb('ff0000'); // All red, in RGB (255, 0, 0)
            const greenrgbLow = converter.hexToRgb('00ff00'); // All green, in RGB (0, 255, 0)
            const bluergbLow = converter.hexToRgb('0000ff'); // All blue, in RGB (0, 0, 255)

            expect(redrgbLow).to.equal('(255, 0, 0)')
            expect(greenrgbLow).to.equal('(0, 255, 0)')
            expect(bluergbLow).to.equal('(0, 0, 255)')

        })
    })
})